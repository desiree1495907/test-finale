import axios from 'axios';
import { useEffect, useContext, useState } from "react";
import { AuthContext } from "../contexts/AuthContext";
import { getUserByEmail } from "../Services/RESTService";
import { checkLoginCookie } from "../Services/RESTService";
import { cookieTypes } from "../Services/config/rest-service-config";
import SearchBar from "../components/SearchBar/SearchBar";
import { fetchWeatherData } from '../Services/RESTService';

import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';

export default function Profile() {
  useEffect(() => {
    document.title = "Profilo";
    getUser();
  }, []);

  const { currentUser, setCurrentUser } = useContext(AuthContext);

  const [profileData, setProfileData] = useState({});

  //se ricarico la pagina, il contesto viene cancellato. In una SPA non si dovrebbe ricaricare le pagine, ma rileggere il token è una soluzione
  const getUser = async () => {
    const userInfo = checkLoginCookie(cookieTypes.jwt);
    if (userInfo != null) {
      const response = await getUserByEmail(userInfo.email);
      const jsonResponse = await response.json();
      setProfileData(jsonResponse);
    }
  };


// ho provato a fare una ricerca delle città utilizzando axios ma devo impleentarla in futuro

  const [weatherData, setWeatherData] = useState(null);
  const [searchTerm, setSearchTerm] = useState([]);


  // funzione per prendere dati da fetchWeatherData
  useEffect(() => {
    async function fetchData() {
      const data = await fetchWeatherData(); 
      setWeatherData(data);
    }
    fetchData();
  }, []);

  const handleSearch = async (term) => {
    try {
      const response = await axios.get(
        `https://api.open-meteo.com/v1/forecast?city=${term}`
      );
      setWeatherData(response.data);
    } catch (error) {
      console.error(
        "Errore durante la ricerca delle informazioni meteo:",
        error
      );
    }
  };

  return (
    <>
      <SearchBar onSearch={handleSearch} />
      <h2>Previsioni Odierne:</h2>


      {/* TABELLA CONTENENTE LE INFO PRESE DAL DB*/}

      <Table aria-label="weather table">
        <TableHead>
          <TableRow>
            <TableCell>Data</TableCell>
            <TableCell>Temperatura Massima</TableCell>
            <TableCell>Temperatura Minima</TableCell>
            <TableCell>Precipitazioni</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {weatherData &&
            weatherData.map((data) => (
              <TableRow key={data.id}>
                <TableCell>{data.time}</TableCell>
                <TableCell>{data.temperature2mMax}</TableCell>
                <TableCell>{data.temperature2mMin}</TableCell>
                <TableCell>{data.precipitationSum}</TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </>
  );
}
