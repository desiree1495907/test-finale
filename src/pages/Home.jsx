import { useEffect } from "react";
import image from "../assets/images/img.png";
import Footer from "../components/Footer/Footer";

export default function Home() {
  useEffect(() => {
    document.title = "Home";
  }, []);

  return (
   <>
      <img src={image} alt="Immagine di benvenuto" />
      <Footer />
    </>
    );
}
