import React, { useState } from 'react';
import './SearchBar.css';

const SearchBar = ({ onSearch }) => {
    const [term, setTerm] = useState('');

    const onSubmit = (event) => {
        event.preventDefault();
        onSearch(term);
    };

    return (
        <div className="search-bar">
            <form onSubmit={onSubmit}>
                <input
                    type="text"
                    placeholder="Cerca città..."
                    value={term}
                    onChange={(e) => setTerm(e.target.value)}
                />
                <button type="submit">Cerca</button>
            </form>
        </div>
    );
};

export default SearchBar;
