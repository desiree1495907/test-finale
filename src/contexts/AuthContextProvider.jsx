import { useState, useEffect } from "react";
import { AuthContext } from "./AuthContext";
import { useOutlet } from "react-router-dom";
import { checkLoginCookie } from "../Services/RESTService";
import { cookieTypes } from "../Services/config/rest-service-config";

//come props si passa il children
export default function AuthContextProvider({children}) {

  //con useState vado ad inizializzare lo stato --> come mi ritorna dal backend
  const [currentUser, setCurrentUser] = useState({nome:'', cognome:'', email:'', ruoli:[]});

  //useEffect entra in azione al primo caricamento
  useEffect(() => {

    //faccio il check se è salvato il cookie di login e vado a salvare i dati
    const loginInfo = checkLoginCookie(cookieTypes.jwt);
    if (loginInfo != null) { //altrimenti l'utente rimane vuoto
      setCurrentUser({
        nome: loginInfo.nome,
        cognome: loginInfo.cognome,
        email: loginInfo.email,
        ruoli: loginInfo.ruoli
      });
    }
  }, []);


  //nomeContesto.Provider, e avvolge tutto quello che c'è all'interno di children, 
  //in value gli passo in questo caso un oggetto (o array) che è la variabile dello stato corrente e la variabile che lo va a modificare
  return (
    <AuthContext.Provider value={{currentUser, setCurrentUser}}> 
      {children}
    </AuthContext.Provider>
  );
}
