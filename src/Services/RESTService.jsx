import { URLs } from './config/rest-service-config'
import Cookies from "js-cookie";
import { jwtDecode } from "jwt-decode";
import { jwtExpirations } from './config/rest-service-config';
import { cookieTypes } from './config/rest-service-config';

//registrazione: fetch
export async function registerUser(formData){
  const jsonData = JSON.stringify(formData)
  //const response = await fetch(URLs.registerUrl, {

    const response = await fetch("http://localhost:8080/api/utente/registrazione", {
    mode: "cors",
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: jsonData
  })
  return response.status
}

//login
export async function loginUser(formData){
  const jsonData = JSON.stringify(formData)
  //const response = await fetch(URLs.loginUrl, {

  const response = await fetch("http://localhost:8080/api/utente/login", {
    mode: "cors",
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: jsonData
  })
  return response
}

//recupero utente tramite email
export async function getUserByEmail(email) {
  const jsonData = JSON.stringify(email)
  const response = await fetch(URLs.getUserbyEmail + `/?email=${email}`, {
    mode: "cors",
    method: "GET",
  })
  return response
}

//recupero tutti gli utenti
export async function getAllUsers(){
  try{
    //const response = await fetch(URLs.getAllUsersUrl, {

    const response = await fetch("http://localhost:8080/api/utente/getUtenti", {
      mode: "cors"
    })
    const allUsersData = await response.json()
    return allUsersData

  }catch(error){
    return null
  }
}




// OPEN-METEO SERVICE: 

export async function fetchWeatherData() {
  try {
    const response = await fetch('http://localhost:8080/api/meteo/all');
    if (!response.ok) {
      throw new Error('Errore durante il recupero dei dati meteo');
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error('Errore durante il recupero dei dati meteo:', error);
    return [];
  }
}



//aggiornamento dati utente
export async function updateUser(formData){
  const jsonData = JSON.stringify(formData)
  //const response = await fetch(URLs.updateUser, {

  const response = await fetch("http://localhost:8080/api/utente/aggiornaUtente", {
    mode: 'cors',
    method: 'PUT',
    headers: {
      "Content-Type": "application/json"
    },
    body: jsonData
  })
  return response.status
}


//cancello utente usando la mail
export async function deleteUser(email) {
  const response = await fetch(`http://localhost:8080/api/utente/eliminaUtenteMail/${email}`, {
      method: 'DELETE',
      headers: {
          'Authorization': getBearerToken(cookieTypes.jwt)
      }
  });

  if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
  } else {
      console.log("Utente eliminato con successo");
  }
}


// --- CONTROLLI ---

//cancello il cookie
export function deleteCookie(type){
  Cookies.remove(type)
}

//salvo il jwt nei cookies
export function setLoginCookie(jwtToken) {
  const jwtString = JSON.stringify(jwtToken.token)
  Cookies.set(cookieTypes.jwt, jwtString, {expires: jwtExpirations.oneMonth})
  return jwtDecode(jwtString)
}

//controllo che esista il jwt e, se esiste, lo decodifico per leggere i dati dell'utente
export function checkLoginCookie(type){
  const loginCookie = Cookies.get(type)
  if(loginCookie != undefined){
    return jwtDecode(loginCookie)
  } else {
    return null
  }
}

//controllo che esista il jwt e, se esiste, lo estraggo concatenato con Bearer
export function getBearerToken(type){
  const loginCookie = Cookies.get(type)
  const shortToken = loginCookie.substring(1, loginCookie.length - 1)
  if(shortToken != undefined){
    return "Bearer " + shortToken
  } else {
    return null
  }
}
